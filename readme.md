# Projet PHP, MySQL, Ajax, Jquery (21/11/2020)

Projet réalisé avec deux camarades, l'objectif étant de créer un site de sondage entre amis. L'objectif de ce site est de proposer à nos amis d'aider l'internaute dans ces choix.

Exemple :
Que vais-je manger aujourd'hui?
1. Japonais,
2. Macdo

Les sondages sont créés sur le site et ont un temps de réponse limité par l'auteur.
L'internaute à sa propre liste d'amis qu'il peut modifier à tous moment. Il pourra voir si ces amis sont connecté ou non.
L'idée est de partager le lien du sondage à ses amis qui pourront répondre s'ils sont connectés. 
L'auteur du sondage verra au fur et à mesure les réponses des résultats s'afficher sans avoir à actualiser la page ainsi que les commentaires publiés sur le sondage.

Pour ce projet différentes pages ont été misent en place:
* Une page d'accueil qui affiche les sondages en cours
* Une page pour répondre, partager le sondage et ajouter des commentaires
* Une page de recherche d'amis pour les ajouter et la liste de nos amis actuels
* Une page de connexion et d'inscription
* Une page profil et de modification des informations de l'utilisateur
* Une page de création de sondage
* Une page de consultation des résultats des sondages passés

3 utilisateurs ont été créer dans la base de donnée pour l'exemple :
* Pseudo : karen, mdp : karen
* Pseudo : alex, mdp : alex
* Pseudo : benji, mdp : benji